# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 09:25:18 2018
simple_example
@author: a5233
"""

from rdflib import Graph, Literal, BNode, RDF
from rdflib.namespace import FOAF, DC

if __name__=='__main__':

    store = Graph()

    # Bind a few prefix, namespace pairs for pretty output
    '''綁定幾個前綴，命名空間對以獲得相當好的輸出'''
    '''bind 綁定'''
    '''.bind("前綴",Namespace) ''' 
    store.bind("dc", DC)
    store.bind("foaf", FOAF)

    # Create an identifier to use as the subject for Donna.
    '''創建一個標識符以用作Donna的主題。'''
    donna = BNode()

    # Add triples using store's add method.
    '''使用儲存庫的添加方法添加三元組。'''
    store.add((donna, RDF.type, FOAF.Person))
    store.add((donna, FOAF.nick, Literal("donna", lang="foo")))
    store.add((donna, FOAF.name, Literal("Donna Fales")))

    # Iterate over triples in store and print them out.
    '''在儲存庫中迭代三元組並打印出來。'''
    print("--- printing raw triples ---")
    for s, p, o in store:
        print(s, p, o)

    # For each foaf:Person in the store print out its mbox property.
    '''對於每個foaf：儲存庫中的人員打印出其mbox屬性。'''
    print("--- printing mboxes ---")
    for person in store.subjects(RDF.type, FOAF["Person"]):
        for mbox in store.objects(person, FOAF["mbox"]):
            print(mbox)

    # Serialize the store as RDF/XML to the file donna_foaf.rdf.
    '''將儲存庫作為RDF / XML序列化到文件donna_foaf.rdf。'''
    '''serialize 序列化'''
    '''.serialize(目的地, format(格式)="", max_depth(深度))'''
    store.serialize("donna_foaf.rdf", format="pretty-xml", max_depth=3)

    # Let's show off the serializers
    '''讓我們展示序列化器'''

    print("RDF Serializations:")

    # Serialize as XML
    print("--- start: rdf-xml ---")
    print(store.serialize(format="pretty-xml"))
    print("--- end: rdf-xml ---\n")

    # Serialize as Turtle
    print("--- start: turtle ---")
    print(store.serialize(format="turtle"))
    print("--- end: turtle ---\n")

    # Serialize as NTriples
    print("--- start: ntriples ---")
    print(store.serialize(format="nt"))
    print("--- end: ntriples ---\n")