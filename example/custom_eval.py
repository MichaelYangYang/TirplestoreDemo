# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 13:26:37 2018
custom_eval.py
@author: a5233
"""

"""
此示例顯示如何添加自定義評估函數來處理某些SPARQL代數元素
添加一個自定義函數，在請求“rdf：type”三元組時添加“rdfs：subClassOf”推理“。
這裡自定義eval函數是手動添加的，通常你會使用setuptools和entry_points來完成它：
i.e. in your setup.py::
    entry_points={'rdf.plugins.sparqleval':['myfunc = mypackage:MyFunction']}
"""

import rdflib

from rdflib.plugins.sparql.evaluate import evalBGP
from rdflib.namespace import FOAF

# any number of rdfs.subClassOf
'''any number of rdfs.subClassOf'''
inferredSubClass = rdflib.RDFS.subClassOf * '*'  



def customEval(ctx, part):
    """
    Rewrite triple patterns to get super-classes
    重寫三重模式以獲得父類
    """

    if part.name == 'BGP':

        # rewrite triples
        triples = []
        for t in part.triples:
            if t[1] == rdflib.RDF.type:
                bnode = rdflib.BNode()
                triples.append((t[0], t[1], bnode))
                triples.append((bnode, inferredSubClass, t[2]))
            else:
                triples.append(t)

        # delegate to normal evalBGP
        return evalBGP(ctx, triples)

    raise NotImplementedError()

if __name__=='__main__':

    # add function directly, normally we would use setuptools and entry_points
    '''直接添加自訂意函數，通常我們會使用setuptools和entry_points'''
    rdflib.plugins.sparql.CUSTOM_EVALS['exampleEval'] = customEval

    g = rdflib.Graph()
    g.load("foaf.rdf")

    # Add the subClassStmt so that we can query for it!
    g.add((FOAF.Person,
           rdflib.RDFS.subClassOf,
           FOAF.Agent))

    # Find all FOAF Agents
    for x in g.query(
            'PREFIX foaf: <%s> SELECT * WHERE { ?s a foaf:Agent . }' % FOAF):
        print(x)