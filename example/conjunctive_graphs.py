# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 10:49:05 2018
conjunctive_graphs.py
@author: a5233
"""
"""
An RDFLib ConjunctiveGraph is an (unnamed) aggregation of all the named graphs
within a Store. The :meth:`~rdflib.graph.ConjunctiveGraph.get_context`
method can be used to get a particular named graph, or triples can be
added to the default graph
This example shows how to create some named graphs and work with the
conjunction of all the graphs.

RDFLib ConjunctiveGraph是Store中所有命名圖的（未命名）聚合。
可以使用：meth：`〜rdflib.graph.ConjunctiveGraph.get_context`
方法來獲取特定的命名圖，或者可以將三元組添加到默認圖。
這個例子展示瞭如何創建一些命名圖並使用所有圖的連接。
"""

from rdflib import Namespace, Literal, URIRef
from rdflib.graph import Graph, ConjunctiveGraph
from rdflib.plugins.memory import IOMemory


if __name__=='__main__':
    
    
    ns = Namespace("http://love.com#")

    mary = URIRef("http://love.com/lovers/mary#")
    john = URIRef("http://love.com/lovers/john#")

    cmary=URIRef("http://love.com/lovers/mary#")
    cjohn=URIRef("http://love.com/lovers/john#")

    store = IOMemory()
    '''ConjunctiveGraph 連接圖'''
    '''ConjunctiveGraph(store=儲存庫, identifier=識別碼) '''
    g = ConjunctiveGraph(store=store)
    g.bind("love",ns)#love為ns的標誌符

    gmary = Graph(store=store, identifier=cmary)

    gmary.add((mary, ns['hasName'], Literal("Mary")))
    gmary.add((mary, ns['loves'], john))

    gjohn = Graph(store=store, identifier=cjohn)
    gjohn.add((john, ns['hasName'], Literal("John")))

    #enumerate contexts
    '''文本上架'''
    '''.context(triple=None) 指定三元組的上下文'''
    for c in g.contexts():
        print("-- %s " % c)
        
    print("===================")  

    #separate graphs
    '''單獨的圖表'''
    print(gjohn.serialize(format='n3'))
    print("===================")
    print(gmary.serialize(format='n3'))
    print("===================")

    #full graph
    '''全圖'''
    print(g.serialize(format='n3'))

    # query the conjunction of all graphs
    '''SPARQL查詢所有圖的連接'''
    print('Mary loves:')
    for x in g[mary : ns.loves/ns.hasName]:
        print(x)
