# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 11:34:12 2018
custom_datatype.py
@author: a5233
"""

"""
RDFLib can map between data-typed literals and python objects.
Mapping for integers, floats, dateTimes, etc. are already added, but
you can also add your own.
This example shows how :meth:`rdflib.term.bind` lets you register new
mappings between literal datatypes and python objects

RDFLib可以在數據類型文字和python對象之間進行映射。
已經添加了整數，浮點數，日期時間等的映射，但您也可以添加自己的。
這個例子顯示瞭如何：meth：`rdflib.term.bind`允許你在文字數據類型
和python對象之間註冊新的映射。
"""


from rdflib import Graph, Literal, Namespace, XSD
from rdflib.term import bind

if __name__=='__main__':

    # complex numbers are not registered by default
    # no custom constructor/serializer needed since
    # complex('(2+3j)') works fine
    '''
    複數不會默認註冊，因為complex（'（2 + 3j）'）正常工作，
    所以不需要自定義構造函數/序列化程序
    '''
    '''
    term.bind 術語約束
    bind(datatype(資料型態),pythontype(python的函數)) 相互映照
    '''
    bind(XSD.complexNumber, complex)

    ns=Namespace("urn:my:namespace:")
    
    c=complex(2,3)
    g=Graph()
    g.add((ns.mysubject, ns.myprop, Literal(complex(2,3))))

    n3=g.serialize(format='n3')

    # round-trip through n3

    g2=Graph()
    g2.parse(data=n3, format='n3')

    l2=list(g2)[0][2]
    print(l2)
    '''回到一個python複雜對象'''
    print(l2.value == c) # back to a python complex object
