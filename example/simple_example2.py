# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 09:25:18 2018
simple_example2
@author: a5233
"""

from rdflib import Graph, Literal, BNode, RDF, Namespace, RDFS

if __name__=='__main__':

    store = Graph()
    SOSA=Namespace("http://www.w3.org/ns/sosa/")
    # Bind a few prefix, namespace pairs for pretty output
    '''綁定幾個前綴，命名空間對以獲得相當好的輸出'''
    '''bind 綁定'''
    '''.bind("前綴",Namespace) ''' 
    store.bind("sosa", SOSA)

    # Create an identifier to use as the subject for Donna.
    '''創建一個標識符以用作Donna的主題。'''
    AAA = BNode()
    BBB = BNode()

    # Add triples using store's add method.
    '''使用儲存庫的添加方法添加三元組。'''
    store.add((AAA, RDF.type, SOSA.Sensor))
    store.add((AAA, SOSA.isHostedBy, BBB))
    store.add((AAA, RDFS.label, Literal("Vaisala HUMICAP H-chip", lang="en")))


    # Serialize the store as RDF/XML to the file donna_foaf.rdf.
    '''將儲存庫作為RDF / XML序列化到文件donna_foaf.rdf。'''
    '''serialize 序列化'''
    '''.serialize(目的地, format(格式)="", max_depth(深度))'''
    store.serialize("donna_foaf.rdf", format="pretty-xml")

    # Let's show off the serializers
    '''讓我們展示序列化器'''

    print("RDF Serializations:")
    print(store.serialize(format="turtle"))


