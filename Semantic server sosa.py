# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 13:33:48 2018
主機：語意伺服器
描述本體：SOSA
@author: a5233
"""

import rdflib
from rdflib.plugins.memory import IOMemory
from SGraph.simplegraph import SimpleGraph
import os

'''SortURI_Triples()用來簡化URI成 > prefix：object 。
example： 
http://www.semanticweb.org/a5233/ontologies/2018/5/rsp#respberry > rsp:respberry
'''
def SortURI_Triples(PREFIX):
     sort_triples=[]
     for s,p,o in asw:  
          for index in PREFIX:
               if prefix[index] in s:
                    s=s.split(PREFIX[index])                    
                    s=('{}:{}'.format(index,s[1].strip('#')))
          for index in PREFIX:
               if prefix[index] in p:
                    p=p.split(PREFIX[index])
                    p=('{}:{}'.format(index,p[1].strip('#')))
          for index in PREFIX:
               if prefix[index] in o:
                    o=o.split(PREFIX[index])
                    o=('{}:{}'.format(index,o[1].strip('#')))
          #sort_triples.append('({},{},{})'.format(s,p,o))
          sort_triples.append((s,p,o))
     return sort_triples

if __name__=='__main__':
    
     '''定義的命名空間'''
     prefix={}
     prefix['owl'] = "http://www.w3.org/2002/07/owl#"
     prefix['rdf'] = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     prefix['rdfs'] = "http://www.w3.org/2000/01/rdf-schema#"
     prefix['rsp'] = "http://www.semanticweb.org/a5233/ontologies/2018/5/rsp"
     prefix['sosa'] = "http://www.w3.org/ns/sosa/"
     prefix['ssn'] = "http://www.w3.org/ns/ssn/"
     prefix['xml'] = "http://www.w3.org/XML/1998/namespace"
     prefix['xsd'] = "http://www.w3.org/2001/XMLSchema#"
     
     owl=rdflib.Namespace(prefix['owl'])#定義owl的命名空間
     rdf=rdflib.Namespace(prefix['rdf'])#定義rdf的命名空間
     rdfs=rdflib.Namespace(prefix['rdfs'])#定義rdfs的命名空間
     rsp=rdflib.Namespace(prefix['rsp'])#定義rsp的命名空間
     sosa=rdflib.Namespace(prefix['sosa'])#定義sosa的命名空間
     ssn=rdflib.Namespace(prefix['ssn'])#定義ssn的命名空間
     xml=rdflib.Namespace(prefix['xml'])#定義xml的命名空間
     xsd=rdflib.Namespace(prefix['xsd'])#定義xsd的命名空間
     
     '''rdf grapy處理'''
     store = IOMemory()#定義儲存空間位置   
     RASP_G = rdflib.graph.ConjunctiveGraph(store=store)#新建ALL_G的全圖(儲存在store中) 
     RASP_G_PATH = os.path.abspath('SGraph/RDF')
     RASP_G.parse("{}/rasp.owl".format(RASP_G_PATH),format='turtle')#從檔案抓取SOSA本體資源
     #RASP_G.serialize("Semantic server.rdf", format="turtle")#序列化存取
     #print(RASP_G.serialize(format='turtle'))#展示全圖
     
     '''simple grapy處理(作為後續推理與查詢用)'''
     simple_RASP_G = SimpleGraph()
     asw=RASP_G.triples((None,None,None))
     triple_transfer=SortURI_Triples(prefix)#轉成simpleTriple的triple格式
     for triple in triple_transfer:
          simple_RASP_G.add(triple)
     simple_RASP_G.save('Semantic server.csv')
     print(list(simple_RASP_G.triples((None,'sosa:observes',None))))
     #print(list(simple_RASP_G.triples(('rsp:temperature',None,None))))     
     
     
     '''search RDF Triples way'''
     
     #asw=RASP_G.triples((rsp["respberry"],None,sosa["Platform"]))
     #simple_RASP_G.add()
     #print(SortUIR_Triples(prefix))#簡化uri成 標誌符：名稱
     

     
     '''SPARQL way'''
     #for o in RASP_G.objects(rsp.respberry):
          #print(o)
     
                    
     
     
     