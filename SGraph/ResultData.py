# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 09:44:47 2018

@author: a5233
"""
from SGraph.simplegraph import SimpleGraph


class Filter:
    
    class Time:
        class Day(SimpleGraph):       
            def now(self,sensor,):
                print('equel')
                
            def after(self,sensor,maximum):
                print('moreThan')
    
            def between(self,sensor,):
                print('between')
                
            #class Hour 
        
            
        
    class Data(SimpleGraph):#繼承SimpleGraph的function
        
        def __init__(self):
            super().__init__()#繼承SimpleGraph的__init__()
            self.sensor=""
        
        def moreThan(self,num):
            print('moreThan')
            data=self.query([(self.sensor,'sosa:madeObservation','?data'),
                             ('?data','sosa:hasSimpleResult','?result')])
            data_filter_s=[]     
            for d in data:
                if float(d['?result']) > float(num):
                    data_filter_s.append(d['?data'])
            return data_filter_s
                
          
        def lessThan(self,num):
            print('lessThan')
            data=self.query([(self.sensor,'sosa:madeObservation','?data'),
                             ('?data','sosa:hasSimpleResult','?result')])
            data_filter_s=[]     
            for d in data:
                if float(d['?result']) < float(num):
                    data_filter_s.append(d['?data'])
            return data_filter_s
            
        def between(self,num1,num2):
            print('between')
            data=self.query([(self.sensor,'sosa:madeObservation','?data'),
                             ('?data','sosa:hasSimpleResult','?result')])
            data_filter_s=[]     
            for d in data:
                if num1 > num2:
                    if (float(d['?result']) < float(num1) and float(d['?result']) > float(num2)):
                        data_filter_s.append(d['?data'])
                if num2 > num1:
                    if (float(d['?result']) < float(num2) and float(d['?result']) > float(num1)):
                        data_filter_s.append(d['?data'])
                if num2 == num1: 
                    print('兩個數值不得相同')
                    break
            return data_filter_s
        
        def equel(self,num):
            print('equel')
            data=self.query([(self.sensor,'sosa:madeObservation','?data'),
                             ('?data','sosa:hasSimpleResult','?result')])
            data_filter_s=[]     
            for d in data:
                if float(d['?result']) == float(num):
                    data_filter_s.append(d['?data'])
            return data_filter_s
        
        def notEquel(self,num):
            print('equel')
            data=self.query([(self.sensor,'sosa:madeObservation','?data'),
                             ('?data','sosa:hasSimpleResult','?result')])
            data_filter_s=[]     
            for d in data:
                if float(d['?result']) != float(num):
                    data_filter_s.append(d['?data'])
            return data_filter_s
    