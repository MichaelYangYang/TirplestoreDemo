# -*- coding: utf-8 -*-
"""
Created on Fri Jun 01 10:10:46 2018
python2
@author: a5233
"""

class InferenceRule:
     def getqueries(self):
          return []
     def maketriples(self,binding):
          return self._maketriples(**binding)
     
class GrandpaRule(InferenceRule):
     def getqueries(self):
          Children_GrandFather = [('?Children','hasFather','?Father'),
                                  ('?Father','hasFather','?GrandFather'),
                                  ('?Children2','hasMother','?Mother'),
                                  ('?Mother','hasFather','?GrandFather')]
          return [Children_GrandFather]
     def _maketriples(self, Children,Children2,Father,Mother,GrandFather):
          return ([(Children, 'hasGrandFather', GrandFather),
                   (Children2, 'hasGrandFather', GrandFather)])

class GrandmaRule(InferenceRule):
     def getqueries(self):
          Children_GrandMather = [('?Children','hasFather','?Father'),
                                  ('?Father','hasMother','?GrandMather'),
                                  ('?Children2','hasMother','?Mother'),
                                  ('?Mother','hasMother','?GrandMather')]
          return [Children_GrandMather]
     def _maketriples(self, Children,Children2,Father,Mother,GrandMather):
          return ([(Children, 'hasGrandMather', GrandMather),
                   (Children2, 'hasGrandMather', GrandMather)])
               