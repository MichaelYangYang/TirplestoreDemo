# -*- coding: utf-8 -*-
"""
Created on Tue May 29 15:48:59 2018
python2
https://github.com/iferminm/Semantic-Web-Thesis/blob/master/Ejemplo%20Triplets/simplegraph.py
@author: a5233
"""
import os
class SimpleGraph():
    
    def __init__(self):
        self._spo = {}
        self._pos = {}
        self._osp = {}
        self.csvpath = os.path.abspath('SGraph/CSV')
    def _addToIndex(self, index, a, b, c):
        #print("fuchion: _addToIndex")
        #print index
        #print type(index)
        #print a
        #print b
        #print c
        if a not in index: index[a] = {b: set([c])}
        else:
            if b not in index[a]: index[a][b] = set([c])
            else: index[a][b].add(c)

    def add(self, sub_pred_obj):
        #print("fuchion: add")
        sub, pred, obj = sub_pred_obj
        self._addToIndex(self._spo, sub, pred, obj)
        self._addToIndex(self._pos, pred, obj, sub)
        self._addToIndex(self._osp, obj, sub, pred)
        #print self._spo
        #print self._pos
        #print self._osp

    def _removeFromIndex(self, index, a, b, c):
        #print("fuchion: _removeFromIndex")
        try:
            bs = index[a]
            cset = bs[b]
            cset.remove(c)
            if len(cset) == 0: del bs[b]
            if len(bs) == 0: del index[a]
        except KeyError:
            pass

    def value(self, sub=None, pred=None, obj=None):
        #print("fuchion: value")
        #add a convenience method for querying a single value of a single triple
        for retSub, retPred, retObj in self.triples((sub, pred, obj)):
            if sub is None: return retSub
            if pred is None: return retPred
            if obj is None: return retObj
        return None

    def triples(self, sub_pred_obj):
        #print("fuchion: triples")
    # check which terms are present in order to use the correct
        sub, pred, obj = sub_pred_obj
        try:
            if sub != None:
                if pred != None:
                    # sub pred obj
                    if obj != None:
                        if obj in self._spo[sub][pred]:
                            yield(sub, pred, obj)
                    # sub pred None
                    else:
                        for retObj in self._spo[sub][pred]:
                            yield(sub, pred, retObj)
                else:
                    # sub None obj
                    if obj != None:
                        for retPred in self._osp[obj][sub]:
                            yield(sub, retPred, obj)
                    else:
                    # sub None None
                        for retPred, objSet in self._spo[sub].items():
                            for retObj in objSet:
                                yield(sub, retPred, retObj)
            else:
                if pred != None:
                    if obj != None:
                        for retSub in self._pos[pred][obj]:
                            yield(retSub, pred, obj)
                    #None pred None
                    else:
                        for retObj, subSet in self._pos[pred].items():
                            for retSub in subSet:
                                yield(retSub, pred, retObj)
                else:
                    # None None obj
                    if obj != None:
                        for retSub, predSet in self._osp[obj].items():
                            for retPred in predSet:
                                yield(retSub, retPred, obj)
                    # None None None
                    else:
                        for retSub, predSet in self._spo.items():
                            for retPred, objSet in predSet.items():
                                for retObj in objSet:
                                    yield(retSub, retPred, retObj)
        except:
            pass



    def remove(self, sub_pred_obj):
        #print("fuchion: remove")
        sub, pred, obj = sub_pred_obj
        triples = list(self.triples((sub, pred, obj)))
        for (delSub, delPred, delObj) in triples:
            self._removeFromIndex(self._spo, delSub, delPred, delObj)
            self._removeFromIndex(self._pos, delPred, delObj, delSub)
            self._removeFromIndex(self._osp, delObj, delSub, delPred)

    def load(self, filename):
        #print("fuchion: load")
        import csv
        file = '{}/{}'.format(self.csvpath,filename)
        with open(file, "r",newline="")as f:
            #print(f)
            reader = csv.reader(f)
            #print(reader)
            for sub, pred, obj in reader:
                self.add((sub, pred, obj))
        

    def save(self, filename):
        #print("fuchion: save")
        import csv
        file = '{}/{}'.format(self.csvpath,filename)
        with open(file,'w',newline='')as f:
            writer = csv.writer(f)
            for sub, pred, obj in self.triples((None, None, None)):
                writer.writerow([sub,pred,obj])
                #writer.writerow([sub.encode("UTF-8"), pred.encode("UTF-8"), obj.encode("UTF-8")])
                
    def update_save(self, filename ,sub_pred_obj):
        #print("fuchion: save")
        import csv
        sub, pred, obj = sub_pred_obj
        file = '{}/{}'.format(self.csvpath,filename)
        with open(file, 'a',newline='')as f:
            writer = csv.writer(f)
            writer.writerow([sub,pred,obj])

    def query(self, clauses):
        #print("fuchion: query")
        bindings = None
        for clause in clauses:
            bpos = {}
            qc = []       # for query clause
            for pos, elem in enumerate(clause):
                if elem.startswith('?'):
                    qc.append(None)
                    bpos[elem] = pos
                else:
                    qc.append(elem)
            pre_results = list(self.triples((qc[0], qc[1], qc[2])))
            if bindings == None:
                bindings = []
                for pre_result in pre_results:
                    binding = {}
                    for var, pos in bpos.items():
                        binding[var] = pre_result[pos]
                    bindings.append(binding)
            else:
                newb = []
                for binding in bindings:
                    for result in pre_results:
                        valid_match = True
                        temp_binding = binding.copy()
                        for var, pos in bpos.items():                            
                            if var in temp_binding:
                                if temp_binding[var] != result[pos]:
                                    valid_match = False
                            else:
                                temp_binding[var] = result[pos]
                        if valid_match: newb.append(temp_binding)
                    bindings = newb
        return bindings
       
    def applyinference(self, rule):
        #print("fuchion: applyinference")
        queries = rule.getqueries()
        bindings = []
        for query in queries:
            bindings += self.query(query)
        for b in bindings:
            newb={}
            for var, pos in b.items():
                 newb[var.strip('?')]=pos#刪除開頭'?'         
            new_triples = rule.maketriples(newb)
            for triple in new_triples:
                self.add(triple)
            