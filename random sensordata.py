# -*- coding: utf-8 -*-
#coding=utf-8
"""
Spyder Editor

亂數生成溫度資料
假設情境：
  感測器每秒生成1筆。
"""


import time,random
from datetime import datetime
from SGraph.simplegraph import SimpleGraph
sg = SimpleGraph()
filename = 'sensordata.csv'
file = "{}/{}".format(sg.csvpath,filename)
f = open(file, 'w')#清除以往的值
for count in range(10):
    Result_Data = random.uniform(25,45)
    Result_Time = datetime.now()
    sg.update_save(filename,['rsp:SHT31-D','sosa:madeObservation','rsp:data/{}'.format(count)])
    sg.update_save(filename,['rsp:data/{}'.format(count),'sosa:resultTime',str(Result_Time)])
    sg.update_save(filename,['rsp:data/{}'.format(count),'sosa:hasSimpleResult',str(Result_Data)])
    time.sleep(0.1)

