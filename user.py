# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 20:48:02 2018

@author: a5233
"""
          
if __name__=='__main__':
    
    from SGraph.simplegraph import SimpleGraph
    from SGraph.ResultData import Filter
    rsp = SimpleGraph()
    rsp.load('Semantic server.csv')
    
    answer = rsp.query([('?platform','rdf:type','sosa:Platform'),
                        ('?platform','sosa:hosts','?sensor')])
    print(answer[0]['?sensor'])
   

    rd = Filter.Data()
    rd.load('sensordata.csv')
    rd.sensor = answer[0]['?sensor']
    
    a = rd.moreThan(40)#大於
    a1 = rd.lessThan(30)#小於
    a2 = rd.between(40,30)#介於
    a3 = rd.equel(42.75461194815769)#等於
    a4 = rd.notEquel(42.75461194815769)#不等於
    print(list(a))
    print(list(a1))
    print(list(a2))
    print(list(a3))
    print(list(a4))

    
